import VueCookies from 'vue-cookies'

export {
	config,
	setCookie,
	getCookie,
	getAll,
	hasCookie,
	remove
};

function config(expireTime, path) {
	if (path) {
		VueCookies.config(expireTime, path);
	} else {
		VueCookies.config(expireTime);
	}
}

function setCookie(key, value) {
	VueCookies.set(key, value);
}

function getCookie(key) {
	return VueCookies.get(key);
}

function getAll() {
	return VueCookies.keys();
}

function hasCookie(key) {
	return VueCookies.isKey(key);
}

function remove(key) {
	VueCookies.remove(key);
}