# Vue Simple App

Just a simple application implemented using Vue.

## Table of contents

1. [Introduction](#markdown-header-introduction)
2. [Prerequisites](#markdown-header-prerequisites)
3. [Creating the Application](#markdown-header-creating-the-application)
4. [Running the Application](#markdown-header-running-the-application)
5. [Dynamic Usage of Components](#markdown-header-dynamic-usage-of-components)
6. [Storage](#markdown-header-storage)

## Introduction

This repository will guide you through the process of creating and setting up a new Vue application.

It is highly recommended that you follow the [Getting Started](https://vuejs.org/v2/guide/) guide on the official website.

**Note:** This repository already provides a partially implemented RESTful calculator.
If you want to skip the setup and/or will be using the provided app, please skip to section [**Running the application**](#markdown-header-running-the-application).

## Prerequisites

Before you begin, make sure your development environment includes [Node.js®](https://nodejs.org/) and an npm package manager (should come with Node.js already).

Since the provided application is a RESTful service, please make sure the [**server**](https://bitbucket.org/pmpalmeida/simplespringrestserver/) is running while testing the application.

### Node.js

Vue requires Node.js version 8.10 or later.

* Run `node -v` in a terminal to check your version.

## Creating the application

We will create an empty folder where the root of our project will be.

Following that we will run the `init` command so `npm` can setup the development environment.
```
npm init -y
```

The `--y` option automatically fills the configuration with the default values.




After `npm` finishes we will proceed to install Laravel Mix, which will allow us to configure Webpack in a fast and easy way, and then copying the example Mix file to the project root
```
**npm install** laravel-mix --save-dev
**np** node_modules/laravel-mix/setup/webpack.mix.js ./
```

Finally we will proceed to install the Vue package and save it as a dependency (this might take a while)
```
npm install vue --save-dev
```

We will use the `install vue` command to setup the development environment.
```
npm install vue --save-dev
```

Just like React, Vue doesn’t handle backend logic or databases; it just creates a frontend build pipeline, so you can use it with any backend you want.

## Running the application

1. Go to the workspace folder.
2. Launch the server by using the command `npm run watch`.

```
cd my-app
npm run watch
```

The `npm run watch` produces static JavaScript and CSS which will then be accessible to the application whenever `index.html` is opened.

The page will not reload if you make edits; on the other hand, it will compile. So in order to see the changes just made, you just need to press `F5` in the page.

## Dynamic Usage of Components

# Dynamic Components

Sometimes, it’s useful to dynamically switch between components. This is made possible by Vue's `<component>` element with the `is` special attribute.

Check the full explanation [here](https://vuejs.org/v2/guide/components.html#Dynamic-Components).

# Async Components

In large applications, we may need to divide the app into smaller chunks and only load a component from the server when it’s needed. Vue allows you to define your component as a factory function that asynchronously resolves your component definition.

Check the full explanation [here](https://vuejs.org/v2/guide/components-dynamic-async.html).

## Storage

Client-side storage can be done with cookies, Local Storage (technically “Web Storage”), IndexedDB, and WebSQL (a deprecated method that should not be used in new projects).

Vue provides support for Web Storage ([Local Storage and Session Storage](https://vuejs.org/v2/cookbook/client-side-storage.html)).

It is possible however, to use Cookies as another mean of Storage (check [vue-cookies](https://www.npmjs.com/package/vue-cookies)).

This war between which one is best has been going on for years. Here's a quick [review](https://scotch.io/@PratyushB/local-storage-vs-session-storage-vs-cookie)

Of course server-side authentication and storage of the user's session details can also be implemented.

* [JWT](https://jwt.io/) (here's a [tutorial](https://jasonwatmore.com/post/2018/07/06/vue-vuex-jwt-authentication-tutorial-example))